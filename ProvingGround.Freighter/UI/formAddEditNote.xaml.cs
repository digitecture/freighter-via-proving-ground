﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProvingGround.Freighter.UI
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class formAddEditNote : Window
    {

        private int _index;
        formFreighter _owner;

        public formAddEditNote(string Input, int Index, formFreighter Owner)
        {
            InitializeComponent();

            text_note.Text = Input;
            _index = Index;
            _owner = Owner;

        }

        private void button_save_Click(object sender, RoutedEventArgs e)
        {
            _owner._plugInsToCopy[_index].Notes = text_note.Text;
            this.Close();
        }

        private void button_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
